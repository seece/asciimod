#python 2.7

import argparse
import re
import copy
import pprint


parser = argparse.ArgumentParser(description='parse exported song files')
parser.add_argument('songpath', help='Song plaintext file.')
parser.add_argument('--vector', action='store_true')
parser.add_argument('--showkeys', action='store_true')

args = parser.parse_args()


path = args.songpath
data = []
with open(path) as f:
	data = f.read()

#print "read %d chars of data from %s" % (len(data), path)
lines = data.split("\n")
#print "is %d lines" % (len(lines))

def split_fields(s):
	return s.split("\t")

indent_regex = re.compile(r'^(\t*)(\S|$)')
def get_indent(s):
	tabfound = indent_regex.search(s)
	indent = 0
	if tabfound:
		indent = len(tabfound.group(1))
	return indent

def read_list(pairs, top_indent):
	last_key = ""
	res = {}
	i = 0

	while i < len(pairs):
		val = pairs[i][0]
		indent = pairs[i][1]

		# end of sublist
		if indent <= top_indent:
			#print ("breaking at %s %d < %d" % (val, indent, top_indent))
			break

		fields = val
		fieldname = fields[0].rstrip().rstrip(":")

		if len(fields) >= 2:
			res[fieldname] = fields[1]
		else:
			# read sublist
			sub = read_list(pairs[i + 1:], indent)
			res[fieldname] = sub
			#print ("SUBLIST %s = %s" % (val, str(sub)))
			i += len(sub) 

		i+=1

	return res
	
def make_line_generator(lines):
	for line in lines:
		yield line


def read_song_fields(lines):
	""" 
	Returns a map where each key in the song maps has a string or a list strings of or a string->string dictionary as value. 
	
	"""
	g = make_line_generator(lines)
	version = g.next()
	#print "version: " + version

	pairs = []

	for l in g:
		if "!ENDMETADATA" in l:
			break

		line = split_fields(l.lstrip())
		line[0] = line[0].rstrip(":")
		pairs.append((line, get_indent(l)))

	fields = read_list(pairs, -1)

	return fields

def clean_up_fields(fields):
	result = fields

	#allowed = ["message_length", "num_orders", ["metadata", "type"], "num_instruments", "num_samples", "num_patterns", "num_subsongs", "num_channels", "num_samples", "duration", ["metadata", "title"]]

	t = result["type"].strip()
	if t == "mod":
		result["type"] = 1
	elif t == "s3m":
		result["type"] = 2
	elif t == "xm":
		result["type"] = 3
	elif t == "it":
		result["type"] = 4

	result["title"] = len(result["title"])

	return result

fields = read_song_fields(lines)
pprint.PrettyPrinter(indent=2).pprint(fields)
cleaned = clean_up_fields(fields)

if args.vector:
	keys = sorted(cleaned.keys()) # python dict aren't sorted
	values = []

	if args.showkeys:
		print "\n".join(str(cleaned).split(","))

	for key in keys:
		values.append(cleaned[key])

	values = map(float, values)
	values = map(str, values)

	print "(" + ", ".join(values) + ")"

else:
	for key, value in cleaned.iteritems():
		print str(key) + " = " + str(value)
