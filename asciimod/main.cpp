#include <vector>
#include <fstream>
#include <iostream>
#include <cstdio>
#include <set>
#include <libopenmpt.hpp>

void print_strings(std::string indent, const std::vector<std::string>& strings, bool indexed = false)
{
	for (int i = 0 ; i < strings.size(); i++) {
		if (indexed)
			std::cout << indent <<  i << ": " << indent << strings[i] << std::endl;
		else
			std::cout << indent << strings[i] << std::endl;
	}
}

void print_usage()
{
	std::cerr << "Usage: asciimod.exe path/to/module.it" << std::endl;
}

int main(int argc, const char* argv[])
{

	using std::cout;
	using std::endl;
	using std::vector;
	using std::string;

	if (argc < 2) {
		print_usage();
		exit(1);
	}

	const std::string version = "asciimodv1";

	//std::ifstream file( argv[1], std::ios::binary );
	//const char* filepath = "E:\\mods\\kaneel\\kl_adtp.it";
	const char* filepath = argv[1];
	std::ifstream file(filepath, std::ios::binary);
	openmpt::module mod(file);
	std::string sep = ":\t";

	cout << version << endl;

	cout << "file" << sep << filepath << endl;

	vector<string> metadata_keys = mod.get_metadata_keys();
	std::set<string> metadata_key_set(metadata_keys.begin(), metadata_keys.end());
	vector<string> keys_to_print;
	keys_to_print.push_back("type");
	keys_to_print.push_back("title");
	
	//cout << "metadata keys:" << endl;
	//print_strings(indent, metadata_keys);


	string indent = "";
	for (int i = 0 ; i < keys_to_print.size(); i++) {
		// Request metadata key contents only if its existence has been reported.
		if (metadata_key_set.find(keys_to_print[i]) == metadata_key_set.end())
			continue;

		std::cout << indent << keys_to_print[i] << ":\t" << mod.get_metadata(keys_to_print[i]) << std::endl;
	}

	std::string message = "";

	if (metadata_key_set.find("message") != metadata_key_set.end())
		message = mod.get_metadata("message");

	cout << "message_length" << sep << message.size() << endl;

	/*
	for (int i = 0 ; i < metadata_keys.size(); i++) {
		std::cout << indent << metadata_keys[i] << ":\t" << mod.get_metadata(metadata_keys[i]) << std::endl;
	}
	*/

	cout << "duration" << sep << mod.get_duration_seconds() << endl;

	#define PRINT_FIELD(field) cout << #field << sep << mod.get_##field() << endl;
	//cout << "channels: " << mod.get_num_subsongs() << endl;
	PRINT_FIELD(num_channels);
	PRINT_FIELD(num_subsongs);
	PRINT_FIELD(num_orders);
	PRINT_FIELD(num_patterns);
	PRINT_FIELD(num_instruments);
	PRINT_FIELD(num_samples);
	#undef PRINT_FIELD

	// string format_pattern_row_channel_command( int32_t pattern, int32_t row, int32_t channel, int command ) const;
	//cout << mod.format_pattern_row_channel_command(0, 0, 0, openmpt::module::command_note) << endl;;
	vector<string> subsong_names	= mod.get_subsong_names();
	vector<string> channel_names	= mod.get_channel_names();
	vector<string> order_names		= mod.get_order_names();
	vector<string> pattern_names	= mod.get_pattern_names();
	vector<string> instrument_names = mod.get_instrument_names();
	vector<string> sample_names		= mod.get_sample_names();

	cout << "subsong_names:" << endl;
	print_strings(indent, subsong_names, true);
	cout << "channel_names:" << endl;
	print_strings(indent, channel_names, true);
	cout << "order_names:" << endl;
	print_strings(indent, order_names, true);
	cout << "pattern_names:" << endl;
	print_strings(indent, pattern_names, true);
	cout << "instrument_names:" << endl;
	print_strings(indent, instrument_names, true);
	cout << "sample_names:" << endl;
	print_strings(indent, sample_names, true);

	cout << "orderlist: ";

	for (int i = 0 ; i < mod.get_num_orders(); i++) {
		if (i > 0)
			cout << ",";
		cout << mod.get_order_pattern(i);
	}

	cout << endl;

	// TODO output pattern data

	cout << "!ENDMETADATA" << endl;
	
	#ifdef _DEBUG
	// asciimod.exe module.mod hold
	if ((argc == 3) && (strcmp(argv[2], "hold") == 0)) {
	std::cerr << "Debug: press enter to quit" << endl;
		getchar();
	}
	#endif

	return 0;
}