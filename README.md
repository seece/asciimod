# asciimod

Currently using r4370 of libopenmpt built with vs2010.
Debug build with /MDD, release build with static CRT /MT.

## Troubleshooting

When compiling libopenmpt I had to rename `C:\Program Files (x86)\Microsoft Visual Studio 10.0\VC\bin\cvtres.exe` to something else (`cvtres_tentacle.exe`) because otherwise the linking step would fail with error

	LINK : fatal error LNK1123: failure during conversion to COFF: file invalid or corrupt
	
More info here http://stackoverflow.com/questions/10888391/error-link-fatal-error-lnk1123-failure-during-conversion-to-coff-file-inval
